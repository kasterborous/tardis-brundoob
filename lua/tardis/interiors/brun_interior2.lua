local T={}
T.Base="base"
T.Name="Brundoob's TARDIS (Short Rotor)"
T.ID="brun_interior2"
T.Interior=
{
	Model="models/brun/extension/brun_int.mdl",
	ExitDistance=850,
	Portal={
		pos=Vector(340.878,0,42.4),
		ang=Angle(0,180,0),
		width=100,
		height=150
	},
	
	Sounds={
		Idle={
			{
				path="drmatt/tardis/default/interior_idle_loop.wav",
				volume=0.3
			}
		},
		Teleport={
			{
				volume=0.1
			}
		},
	},
	Light=
	{
		color=Color(50,70,250),
		warncolor=Color(255,0,0),
		pos=Vector(0,0,140),
		brightness=3.5
	},

	Lights=
	{
		{
			color=Color(120,150,250),
			warncolor=Color(255,0,0),
			pos=Vector(0,0,-150),
			brightness=4,
		},
		{
			color=Color(120,150,250),
			warncolor=Color(255,0,0),
			pos=Vector(0,0,120),
			brightness=0.5,
			nopower=true
		},
	},

	Fallback=
	{
		pos=Vector(319,0,0.1),
		ang=Angle(0,90,0)
	},
	ScreensEnabled=false,
	Screens=
	{
		{
			pos=Vector(18.82,14.60,59.2),
			ang=Angle(0,150,70),
			width=288,
			height=160,
			visgui_rows=2,
			power_off_black = false
		}
	},
	Sequences="brun_sequence",
	Seats=
	{
		{
			pos=Vector(117,-67,14),
			ang=Angle(0,60,0)
		},
		{
			pos=Vector(0,-135,14),
			ang=Angle(0,0,0)
		},
		{
			pos=Vector(-117,-67,14),
			ang=Angle(0,300,0)
		},
		{
			pos=Vector(-117,67,14),
			ang=Angle(0,240,0)
		},
		{
			pos=Vector(0,137,14),
			ang=Angle(0,180,0)
		},
		{
			pos=Vector(117,67,14),
			ang=Angle(0,120,0)
		},
	},
	Controls = {
		brun_throttle = "teleport",
		brun_screen = nil,
		brun_alever1 = "flight",
		brun_alever5 = "cloak",

	},
	TipSettings=
	{
		style="brun",
        view_range_min=50,
        view_range_max=65,
	},
	CustomTips=
	{
		{pos=Vector(21.454, 37.204, 41.837), text="Coordinates", down = true, right = true},
		{pos=Vector(28.286, 25.297, 43.878), text="Speaker Controls", down = true, right = true, part = "brun_bsliders"},
		{pos=Vector(7.817, 37.251, 43.787), text="Keypad", down = true, part = "brun_bkeypad"},
		{pos=Vector(-11.712, 20.342, 48.556), text="Vortex Shields", part = "brun_csliders"},
		{pos=Vector(-40,0,45), text="Throttle", down = true, part = "brun_throttle"},
		{pos=Vector(-40.251, -13.025, 44.904), text="Handbrake", part = "brun_handbrake"},
		{pos=Vector(-33.643, 11.188, 44.931), text="Locking-down Mechanism", right = true, part = "brun_dpull"},
		{pos=Vector(-42.872, 9.688, 41.169), text="Engine Release", part = "brun_dswitches"},
		{pos=Vector(-20.033, 5.653, 49.779), text="Vortex Flightmode", down = true},
		--{pos=Vector(-20.033, 2.436, 49.779), text="Hi Ryan", part = "dpull3"},
		{pos=Vector(-20.033, -2.436, 49.779), text="Fast Return", right = true, part = "brun_dpull4"},
		{pos=Vector(-20.033, -5.653, 49.779), text="TARDIS Repair", right = true, down = true, part = "dpull5"},
		{pos=Vector(38, 2.387, 44.2), text="Flight"},
		{pos=Vector(38, 5.196, 44.2), text="Anti Gravs", right = true},
		{pos=Vector(38, 13.607, 44.2), text="Cloaking", right = true},
		{pos=Vector(43, 0, 42), text="Isomorphic Controls"},
		{pos=Vector(-42.872, 17.688, 41.169), text="Doors", part = "brun_dtoggles1", down = true},
		{pos=Vector(-42.872, 15.088, 41.169), text="Lock", part = "brun_dtoggles2", right = true, down = true},
		{pos=Vector(43, 17.115, 42), text="Power", part = "brun_aswitch3"},
		{pos=Vector(-13.438, -23.293, 48.093), text="Scanner", right = true, down = true, part = "brun_escreen"},
		{pos=Vector(-17.059, -39.414, 41.382), text="H.A.D.S", part = "brun_etoggles"},
		{pos=Vector(-6.822, -45.619, 43.008), text="Sonic Charger"},
		{pos=Vector(17.128, -28.923, 45.882), text="Exterior View", part = "brun_flightscreen"},
    },
	Parts=
	{
			brun_console=true,
			brun_consoletop=true,
			--brun_rotor=true,
			brun_rotor2=true,
			brun_innerrotor=true,
			--brun_innerrotor2=true,
			brun_pillars=true,
			brun_chairs=true,
		--Panel A
			brun_alever1=true,
			brun_alever2=true,
			brun_alever3=true,
			brun_alever4=true,
			brun_alever5=true,
			brun_aswitch1=true,
			brun_aswitch2=true,
			brun_aswitch3=true,
			brun_alight1=true,
			brun_alight2=true,
			brun_alight3=true,
			brun_alight4=true,
			brun_alight5=true,
			brun_alight6=true,
			brun_abuttons=true,
			brun_acog1=true,
			brun_acog2=true,
			brun_adials=true,
			brun_asliders=true,
			brun_atoggles=true,
		--Panel B
			brun_blevers1=true,
			brun_blevers2=true,
			brun_blevers3=true,
			brun_blevers4=true,
			brun_blevers5=true,
			brun_blevers6=true,
			brun_blevers7=true,
			brun_blevers8=true,
			brun_bkeyboard=true,
			brun_bkeypad=true,
			brun_bsliders=true,
			brun_bswitch1=true,
			brun_bswitch2=true,
			brun_bscreen=true,
			brun_blights=true,
		--Panel C
			brun_cswitches=true,
			brun_cbuttons=true,
			brun_cbuttons_square=true,
			brun_cswitches=true,
			brun_cmeters=true,
			brun_csliders=true,
		--Panel D
			brun_dswitches=true,
			brun_dlights=true,
			brun_dpull=true,
			brun_dpull2=true,
			brun_dpull3=true,
			brun_dpull4=true,
			brun_dpull5=true,
			brun_dtoggles1=true,
			brun_dtoggles2=true,
			brun_handbrake=true,
			brun_throttle=true,
		--Panel E
			brun_ebuttons=true,
			brun_escreen=true,
			brun_eslidersbase=true,
			brun_esliders1=true,
			brun_esliders2=true,
			brun_esliders3=true,
			brun_etoggles=true,
			brun_escanner=true,
			brun_echarger=true,
		--Panel F
			brun_flightscreen=true,
		--Symbols
			brun_rassilon_big=true,
			brun_rassilon=true,
			brun_corridors=true,
			brun_stairwell=true,
			brun_bedrooms=true,
			brun_inaccessible1=true,
			brun_inaccessible2=true,
		--Doors
			brun_door1=true,
			brun_door2=true,
			brun_door3=true,
			brun_door4=true,
			brun_door5=true,
			brun_doorroom1=true,
			brun_doorroom2=true,
			brun_doorroom3=true,
			brun_doorroom4=true,
			brun_doorportal=true,
			brun_doors_locked=true,
		--Misc
			brun_bscreendoor=true,
			brun_microphone=true,
			brun_speakers=true,
			brun_roundels=true,
			brun_doorframes=true,
			brun_stairs=true,
			brun_hallway=true,
			brun_living=true,
			brun_black=true,
	
			door=
			{
					model="models/brun/box/doors_back.mdl",
					posoffset=Vector(30,0,-50),
			}
	},
}
T.Exterior=
{
	Model="models/brun/box/base.mdl",
	Mass=5000,
	ScannerOffset=Vector(25.6,0,63),
	Portal=
	{
		pos=Vector(30,0,50),
		width=48,
		height=94
	},
	
	Fallback=
	{
		pos=Vector(52,0,15),
		ang=Angle(0,90,0)
	},
	
	Light=
	{
		enabled=true,
		pos=Vector(0,0,126),
		color=Color(255,240,160)
	},
	
	Sounds=
	{
		Teleport=
		{
			demat="brun/extension/demat.wav",
			mat="brun/extension/mat.wav"
		},
		Door=
		{
			enabled=true,
			open="brun/extension/doorsopen.wav",
			close="brun/extension/doorsclose.wav"
		},
	},
	
	Parts=
	{
		door=
		{
			model="models/brun/box/doors.mdl",
					posoffset=Vector(-30,0,-50),
		},
	}
}

TARDIS:AddInterior(T)