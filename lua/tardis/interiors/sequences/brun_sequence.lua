local Seq = {
    ID = "brun_sequence",

    ["brun_bkeypad"] = {
        Controls = {
            "brun_etoggles",
            "brun_csliders",
            "brun_handbrake",
            "brun_throttle"
        },
        OnFinish = function(self)
        self.exterior:SetData("handbrake", false)
        if self.exterior:GetData("vortex") then return end
            if IsValid(self) and IsValid(self) then
                self.exterior:Demat()
            end
        end
    },
        ["brun_dpull4"] = {
        Controls = {
            --"brun_csliders",
            "brun_throttle"
        },
        OnFinish = function(self)
            TARDIS:Control("fastreturn", self:GetCreator())
        end
    }
}

TARDIS:AddControlSequence(Seq)