local PART={}
PART.ID = "brun_doorroom3"
PART.Name = "Brundoob Door Room3"
PART.Model = "models/brun/extension/door_room3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.BypassIsomorphic = true

if SERVER then
	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Use()
		sound.Play("brun/extension/intslidedoors.wav", self:LocalToWorld(Vector(-280,0,75)))
		if ( self:GetOn() ) then
			self:Collide( true )
		else
			self:DontCollide( true )
		end
	end
end

TARDIS:AddPart(PART,e)