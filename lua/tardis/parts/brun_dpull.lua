local PART={}
PART.ID = "brun_dpull"
PART.Name = "Brundoob D_Pull"
PART.Model = "models/brun/extension/d_pull.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Message = ""
PART.booleanValue = true

if SERVER then
	function PART:Use(ply)
	    TARDIS:Control("physlock", ply)
	    self:EmitSound( "brun/extension/d_pull.wav" )
	end
end

TARDIS:AddPart(PART,e)