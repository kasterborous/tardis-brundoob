local PART={}
PART.ID = "brun_alever2"
PART.Name = "Brundoob A_Lever2"
PART.Model = "models/brun/extension/a_lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.8

if SERVER then
	function PART:Use(ply)
		TARDIS:Control("float", ply)
		self:EmitSound( "brun/extension/a_levers.wav" )
	end
	function PART:Think()
		if not ( self:GetOn() ) or not self.exterior:GetData("power-state") then
			if self:GetSkin()==0 then return end
			self:SetSkin (0)
		elseif self:GetSkin ()~=1 then
			self:SetSkin(1)
		end
	end
end

TARDIS:AddPart(PART,e)