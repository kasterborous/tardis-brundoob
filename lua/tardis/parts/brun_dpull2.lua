local PART={}
PART.ID = "brun_dpull2"
PART.Name = "Brundoob D_Pull 2"
PART.Model = "models/brun/extension/d_pull2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Message = ""

if SERVER then
	function PART:Use(ply)
	    self:EmitSound( "brun/extension/d_pull.wav" )
	    TARDIS:Control("vortex_flight", ply)
	end
end
--[[
if CLIENT then
	function PART:Initialize()
		self.lever = {}
		self.lever.pos = 0
		self.lever.endpos = 0
	end

	function PART:Think()
		local exterior=self.exterior
		if self.lever.pos == 0 and  BRUN_TARDIS.IS_FAST_REMAT_ON(exterior) then
			self.lever.endpos = 1
		elseif self.lever.pos == 1 and not BRUN_TARDIS.IS_FAST_REMAT_ON(exterior) then
			self.lever.endpos = 0
		end	
		self.lever.pos = math.Approach(self.lever.pos, self.lever.endpos, FrameTime() * self.AnimateSpeed)
		self:SetPoseParameter("switch", self.lever.pos)
	end
end
]]--
TARDIS:AddPart(PART,e)