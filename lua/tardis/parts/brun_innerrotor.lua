local PART={}
PART.ID = "brun_innerrotor"
PART.Name = "Brundoob Inner Rotor"
PART.Model = "models/brun/extension/inner_rotor.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true


if CLIENT then
	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()
		local exterior=self.exterior
		if (self.timerotor.pos>0 and not exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) or (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
				if self.timerotor.pos==0 then
					self.timerotor.mode=1
				elseif self.timerotor.pos==1 and (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
					self.timerotor.pos=0
				end
				
			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.45 )
			self:SetPoseParameter( "switch", self.timerotor.pos )
		end
			if self.exterior:GetData("power-state") and not self.exterior:GetData("health-warning") then
				if self:GetSkin()==0 then return end
				self:SetSkin (0)
			elseif self.exterior:GetData("power-state") and self.exterior:GetData("health-warning") then
				if self:GetSkin()==2 then return end
				self:SetSkin(2)
			else
				if self:GetSkin ()==1 then return end
				self:SetSkin(1)
		end
	end
end

TARDIS:AddPart(PART)