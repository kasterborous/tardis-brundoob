local PART={}
PART.ID = "brun_handbrake"
PART.Name = "Brundoob Handbrake"
PART.Model = "models/brun/extension/handbrake.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.8
PART.Message = ""

if SERVER then
	function PART:Use(ply)
		self:EmitSound( "brun/extension/handbrake.wav" )
		TARDIS:Control("handbrake", ply)
	end
end


TARDIS:AddPart(PART,e)