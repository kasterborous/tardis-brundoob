local PART={}
PART.ID = "brun_flightscreen"
PART.Name = "Brundoob Flightscreen"
PART.Model = "models/brun/extension/flightscreen.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.BypassIsomorphic = true

if SERVER then    
    function PART:Use(ply)
		TARDIS:Control("thirdperson", ply)
    end
if SERVER then
	function PART:Think()
		if self.exterior:GetData("power-state") and not self.exterior:GetData("health-warning") then
			if self:GetSkin()==0 then return end
			self:SetSkin (0)
		elseif self.exterior:GetData("power-state") and self.exterior:GetData("health-warning") then
			if self:GetSkin()==2 then return end
			self:SetSkin(2)
		else
			if self:GetSkin ()==1 then return end
			self:SetSkin(1)
		end
	end
end
end

TARDIS:AddPart(PART,e)