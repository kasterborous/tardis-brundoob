local PART={}
PART.ID = "brun_bswitch2"
PART.Name = "Brundoob B_Switch2"
PART.Model = "models/brun/extension/b_switch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
	function PART:Use()
		self:EmitSound( "brun/extension/a_switch.wav" )
		local part = self.interior:GetPart("brun_bscreen" )
		part:SetOn(not part:GetOn())
		if self.interior:GetScreensOn() and not part:GetOn() then
			self.interior:SetScreensOn(false)
		end
		local part = self.interior:GetPart("brun_bscreendoor")
		part:SetOn(not part:GetOn())
	end
end

TARDIS:AddPart(PART,e)