local PART={}
PART.ID = "brun_throttle"
PART.Name = "Brundoob Throttle"
PART.Model = "models/brun/extension/throttle.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.2
PART.Message = ""
PART.Sound = "brun/extension/throttle.wav"

if SERVER then
	function PART:Use(ply)
		local teleport = self.exterior:GetData("teleport", false)
		local vortex = self.exterior:GetData("vortex", false)
		if (teleport or vortex) then
			self.exterior:Mat()
		end
		if self.interior:GetSequencesEnabled() then return end
		if not (teleport or vortex) then
			self.exterior:Demat()
		end
	end
end
--[[
if CLIENT then
	function PART:Initialize()
		self.throttle = {}
		self.throttle.pos = 0
		self.throttle.endpos = 0
	end

	function PART:Think()
		local exterior=self.exterior
		if BRUN_TARDIS.IS_TELEPORTING(exterior) and not BRUN_TARDIS.IS_IN_VORTEX(exterior) then
			self.throttle.endpos = 1
		elseif BRUN_TARDIS.IS_IN_VORTEX(exterior) and not BRUN_TARDIS.IS_TELEPORTING(exterior) then
			self.throttle.endpos = 1
		elseif self.throttle.pos == 1 and BRUN_TARDIS.IS_TELEPORTING(exterior) and not BRUN_TARDIS.IS_IN_VORTEX(exterior) then
			self.throttle.endpos = 0
		elseif self.throttle.pos == 0 and BRUN_TARDIS.IS_TELEPORTING(exterior) and BRUN_TARDIS.IS_IN_VORTEX(exterior) then
			self.throttle.endpos = 1
		elseif self.throttle.pos == 1 and not BRUN_TARDIS.IS_TELEPORTING(exterior) then
			self.throttle.endpos = 0
		end	

		self.throttle.pos = math.Approach(self.throttle.pos, self.throttle.endpos, FrameTime() * self.AnimateSpeed)
		self:SetPoseParameter("switch", self.throttle.pos)
	end
end
]]--
TARDIS:AddPart(PART,e)