local PART={}
PART.ID = "brun_escreen"
PART.Name = "Brundoob E_Screen"
PART.Model = "models/brun/extension/e_screen.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	function PART:Think()
		if self.exterior:GetData("power-state") then
			if self:GetSkin()==0 then return end
			self:SetSkin (0)
		elseif self:GetSkin ()~=1 then
			self:SetSkin(1)
		end
	end
	function PART:Use(ply)
    	--self:EmitSound( "brun/extension/keyboard.wav" )
		TARDIS:Control("scanner", ply)
    end
end

TARDIS:AddPart(PART,e)