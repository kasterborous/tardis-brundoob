local PART={}
PART.ID = "brun_roundels"
PART.Name = "Brundoob Roundels"
PART.Model = "models/brun/extension/roundels.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	function PART:Think()
		if self.exterior:GetData("power-state") and not self.exterior:GetData("health-warning") then
			if self:GetSkin()==0 then return end
			self:SetSkin (0)
		elseif self.exterior:GetData("health-warning") then
			if self:GetSkin()==2 then return end
			self:SetSkin(2)
		else
			if self:GetSkin ()==1 then return end
			self:SetSkin(1)
		end
	end
end

TARDIS:AddPart(PART)