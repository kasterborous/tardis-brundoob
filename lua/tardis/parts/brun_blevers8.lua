local PART={}
PART.ID = "brun_blevers8"
PART.Name = "Brundoob B_Lever8"
PART.Model = "models/brun/extension/b_levers8.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
	function PART:Use()
		self:EmitSound( "brun/extension/b_levers.wav" )
		local part = self.interior:GetPart("brun_console" )
		part:SetOn(not part:GetOn())
		local part = self.interior:GetPart("brun_console")
		part:SetOn(not part:GetOn())
	end
end

TARDIS:AddPart(PART,e)