local PART={}
PART.ID = "brun_dpull3"
PART.Name = "Brundoob D_Pull 3"
PART.Model = "models/brun/extension/d_pull3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5

if SERVER then
	function PART:Use(ply)
		self:EmitSound( "brun/extension/d_pull.wav" )
	end
end

TARDIS:AddPart(PART,e)