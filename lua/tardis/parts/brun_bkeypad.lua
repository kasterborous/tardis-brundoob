local PART={}
PART.ID = "brun_bkeypad"
PART.Name = "Brundoob B_Keypad"
PART.Model = "models/brun/extension/b_keypad.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	function PART:Use()
		self:EmitSound( "brun/extension/keyboard.wav" )
	end
end

TARDIS:AddPart(PART,e)