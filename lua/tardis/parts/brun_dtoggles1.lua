local PART={}
PART.ID = "brun_dtoggles1"
PART.Name = "Brundoob D_Toggles1"
PART.Model = "models/brun/extension/d_toggles1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
	function PART:Use(ply)
		self:EmitSound( "brun/extension/a_switch.wav" )
		self:SetUseType(SIMPLE_USE)
		self.exterior:ToggleDoor()
	end

	function PART:Think()
		local doorState = (self.exterior:GetData("doorstatereal") or false)
		if doorState then
			self:SetSkin( 1 )
		else
			self:SetSkin( 0 )
		end
	end
end

--[[if CLIENT then
	function PART:Initialize()
		self.lever = {}
		self.lever.pos = 0
		self.lever.endpos = 0
	end

	function PART:Think()
		if self.lever.pos == 0 and self.exterior:GetData("doorstate") then
			self.lever.endpos = 1
		elseif self.lever.pos == 1 and not self.exterior:GetData("doorstate") then
			self.lever.endpos = 0
		end
		self.lever.pos = math.Approach(self.lever.pos, self.lever.endpos, FrameTime() * self.AnimateSpeed)
		self:SetPoseParameter("switch", self.lever.pos)
	end
end]]

TARDIS:AddPart(PART,e)