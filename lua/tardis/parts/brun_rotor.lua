local PART={}
PART.ID = "brun_rotor"
PART.Name = "Brundoob Rotor"
PART.Model = "models/brun/extension/rotor.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Collision = true

TARDIS:AddPart(PART)