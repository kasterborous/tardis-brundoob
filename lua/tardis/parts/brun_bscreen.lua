local PART={}
PART.ID = "brun_bscreen"
PART.Name = "Brundoob B_Screen"
PART.Model = "models/brun/extension/b_screen.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1

if SERVER then
	function PART:Think()
		if self.exterior:GetData("power-state") then
			if self:GetSkin()==1 then return end
			self:SetSkin (1)
			elseif self:GetSkin ()~=0 then
			self:SetSkin(0)
		end
	end
end
TARDIS:AddPart(PART,e)