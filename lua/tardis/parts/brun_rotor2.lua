local PART={}
PART.ID = "brun_rotor2"
PART.Name = "Brundoob Rotor2"
PART.Model = "models/brun/extension/rotor2.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Collision = true

if CLIENT then
	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()
		local exterior=self.exterior
		if self.timerotor.pos>0 and not exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") or exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") then
				if self.timerotor.pos==0 then
					self.timerotor.mode=1
				elseif self.timerotor.pos==1 and (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
					self.timerotor.pos=0
				end
				
			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.45 )
			self:SetPoseParameter( "switch", self.timerotor.pos )
		end
	end
end

TARDIS:AddPart(PART)