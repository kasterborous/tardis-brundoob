local PART={}
PART.ID = "brun_blevers1"
PART.Name = "Brundoob B_Lever1"
PART.Model = "models/brun/extension/b_levers1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
	function PART:Use()
		self:EmitSound( "brun/extension/b_levers.wav" )
	end
end

TARDIS:AddPart(PART,e)