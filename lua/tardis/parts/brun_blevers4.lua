local PART={}
PART.ID = "brun_blevers4"
PART.Name = "Brundoob B_Lever4"
PART.Model = "models/brun/extension/b_levers4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
	function PART:Use()
		self:EmitSound( "brun/extension/b_levers.wav" )
	end
	function PART:Think()
		local part = self.interior:GetPart("brun_blights")
		if ( self:GetOn() ) and self.exterior:GetData("power-state") then
			part:SetSkin( 1 )
		else
			part:SetSkin( 0 )
		end
	end
end

TARDIS:AddPart(PART,e)