local PART={}
PART.ID = "brun_blevers7"
PART.Name = "Brundoob B_Lever7"
PART.Model = "models/brun/extension/b_levers7.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
	function PART:Use()
		self:EmitSound( "brun/extension/b_levers.wav" )
	end
end

TARDIS:AddPart(PART,e)